package com.nerdery.prisonersdilemma;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static java.nio.file.StandardOpenOption.*;

/**
 * Created by ryanvanbelkum on 12/19/15.
 */
public class PrisonerFactory {

    Path path = Paths.get("./file.txt");

    public Prisoner getPrisoner(Interrogation interrogation) {
        Prisoner prisoner;
        if (confessBasedOnOpponentHistory(interrogation) || interrogation.getPartnerPreviousResponse() == null) {
            prisoner = new RatPrisoner();
        } else {
            prisoner = new SilentPrisoner();
        }

        StringBuilder sb = new StringBuilder(interrogation.getPartnerName()).append(",")
                .append(interrogation.getPartnerDiscipline()).append(",")
                .append(interrogation.getPartnerPreviousResponse()).append(",")
                .append(prisoner.doesConfess() ? Response.confess : Response.silent).append("\n");
        writeToFile(sb.toString());
        return prisoner;
    }

    private void writeToFile(String outText) {
        try (OutputStream out = new BufferedOutputStream(
                Files.newOutputStream(path, CREATE, APPEND))) {
            out.write(outText.getBytes());
        } catch (IOException x) {
            System.err.println(x);
        }
    }

    private Boolean confessBasedOnOpponentHistory(Interrogation interrogation) {
        int lineCount = 0;
        int confessCount = 0;
        int confessInARow = 0;
        Boolean returnBool = false;

        try (BufferedReader br = Files.newBufferedReader(path)) {
            String line;
            while ((line = br.readLine()) != null) {
                List<String> lineList = Arrays.asList(line.split(","));
                if (lineList.get(0).equals(interrogation.getPartnerName()) &&
                        lineList.get(1).equals(interrogation.getPartnerDiscipline()) &&
                        lineList.get(2).equals(Response.confess.toString())) {
                    confessCount++;
                    confessInARow++;
                } else {
                    confessInARow = 0;
                }
                lineCount++;
            }
            br.close();
        } catch (IOException e) {
        }

        if (confessCount > 0) {
            Float perc = (confessCount * 100.0f) / lineCount;
            if (perc >= 50 || confessInARow >= 3) { // find out chance of them ratting you out
                returnBool = true;
            }
        }
        return returnBool;
    }
}
