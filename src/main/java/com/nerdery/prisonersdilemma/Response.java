package com.nerdery.prisonersdilemma;

/**
 * Created by ryanvanbelkum on 12/19/15.
 */
public enum Response {
    confess, silent
}
