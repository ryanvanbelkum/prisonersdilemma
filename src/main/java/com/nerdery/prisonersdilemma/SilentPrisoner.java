package com.nerdery.prisonersdilemma;

/**
 * A prisoner who always confesses.
 *
 * @author Josh Klun (jklun@nerdery.com)
 */
public class SilentPrisoner implements Prisoner {

    @Override
    public boolean doesConfess() {
        return false;
    }
}
