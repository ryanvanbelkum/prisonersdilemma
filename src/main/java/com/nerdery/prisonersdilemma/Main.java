package com.nerdery.prisonersdilemma;

/**
 * Created by ryanvanbelkum on 12/19/15.
 */
public class Main {

    public static void main(String[] args) {
        // 0 = partnerName , 1 = partnerDiscipline , 2 = partnerPreviousResponse , 3 playerPreviousResponse

        Interrogation interrogation;
        if (args.length < 2){
            System.out.println("USAGE = 0 = partnerName , 1 = partnerDiscipline , 2 = partnerPreviousResponse , 3 playerPreviousResponse");
            System.exit(0);
        }
        if (args.length > 2) {
            interrogation = new Interrogation(args[0], args[1], Response.valueOf(args[2].toLowerCase()), Response.valueOf(args[3].toLowerCase()));
        }else {
            interrogation = new Interrogation(args[0], args[1]);
        }

        PrisonerFactory prisonerFactory = new PrisonerFactory();
        Prisoner prisoner = prisonerFactory.getPrisoner(interrogation);
        System.out.println(prisoner.doesConfess() ? Response.confess : Response.silent);
    }

}
