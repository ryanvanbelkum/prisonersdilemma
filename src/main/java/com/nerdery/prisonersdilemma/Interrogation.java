package com.nerdery.prisonersdilemma;

/**
 * Created by ryanvanbelkum on 12/19/15.
 */
public class Interrogation {

    private String partnerName;
    private String partnerDiscipline;
    private Response partnerPreviousResponse;
    private Response playerPreviousResponse;

    public Interrogation(String partnerName, String partnerDiscipline) {
            this(partnerName, partnerDiscipline, null, null);
    }

    public Interrogation(String partnerName, String partnerDiscipline,
                         Response partnerPreviousResponse, Response playerPreviousResponse) {
        this.partnerName = partnerName;
        this.partnerDiscipline = partnerDiscipline;
        this.partnerPreviousResponse = partnerPreviousResponse;
        this.playerPreviousResponse = playerPreviousResponse;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getPartnerDiscipline() {
        return partnerDiscipline;
    }

    public void setPartnerDiscipline(String partnerDiscipline) {
        this.partnerDiscipline = partnerDiscipline;
    }

    public Response getPartnerPreviousResponse() {
        return partnerPreviousResponse;
    }

    public void setPartnerPreviousResponse(Response partnerPreviousResponse) {
        this.partnerPreviousResponse = partnerPreviousResponse;
    }

}
